// ActionPage.cpp : implementation file
//

#include "stdafx.h"
#include "HMS PackageManager.h"
#include "ActionPage.h"


// CActionPage dialog

IMPLEMENT_DYNAMIC(CActionPage, CPropertyPage)

CActionPage::CActionPage()
	: CPropertyPage(CActionPage::IDD)
{
    m_psp.dwFlags |= PSP_DEFAULT|PSP_USEHEADERTITLE|PSP_USEHEADERSUBTITLE;
    m_psp.pszHeaderTitle = _T("Title");
    m_psp.pszHeaderSubTitle = _T("And subtitle");
}

CActionPage::~CActionPage()
{
}

void CActionPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CActionPage, CPropertyPage)
	ON_WM_PAINT()
END_MESSAGE_MAP()


// CActionPage message handlers
BOOL CActionPage::OnInitDialog()
{
	CPropertyPage::OnInitDialog();

	return TRUE;
}

BOOL CActionPage::OnSetActive()
{
	CPropertySheetEx* pSheet = (CPropertySheetEx*)GetParent();
	pSheet->SetWizardButtons( PSWIZB_BACK | PSWIZB_NEXT /* | PSWIZB_FINISH */ /* | PSWIZB_DISABLEDFINISH */);
	return CPropertyPage::OnSetActive();
}

BOOL CActionPage::OnKillActive()
{
	return CPropertyPage::OnKillActive();
}

LRESULT CActionPage::OnWizardNext()
{
	return CPropertyPage::OnWizardNext();
}

void CActionPage::OnPaint()
{
	CPaintDC dc(this); // device context for painting

}
