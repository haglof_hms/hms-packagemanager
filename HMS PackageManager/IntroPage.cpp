// IntroPage.cpp : implementation file
//

#include "stdafx.h"
#include "HMS PackageManager.h"
#include "IntroPage.h"


// CIntroPage dialog

IMPLEMENT_DYNAMIC(CIntroPage, CPropertyPage)

//CIntroPage::CIntroPage() : CPropertyPage(CIntroPage::IDD, 0, IDS_HEADERTITLE1, IDS_HEADERSUBTITLE1)
CIntroPage::CIntroPage()
	: CPropertyPage(CIntroPage::IDD)
{
	m_psp.dwFlags |= PSP_DEFAULT|PSP_HIDEHEADER;
}


CIntroPage::~CIntroPage()
{
}

void CIntroPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CIntroPage, CPropertyPage)
END_MESSAGE_MAP()


// CIntroPage message handlers
BOOL CIntroPage::OnInitDialog()
{
	CPropertyPage::OnInitDialog();

	return TRUE;
}

BOOL CIntroPage::OnSetActive()
{
	CPropertySheetEx* pSheet = (CPropertySheetEx*)GetParent();
	pSheet->SetWizardButtons( /*PSWIZB_BACK |*/ PSWIZB_NEXT /* | PSWIZB_FINISH */ /* | PSWIZB_DISABLEDFINISH */);
	return CPropertyPage::OnSetActive();
}

BOOL CIntroPage::OnKillActive()
{
	return CPropertyPage::OnKillActive();
}

LRESULT CIntroPage::OnWizardNext()
{
	return CPropertyPage::OnWizardNext();
}

