#pragma once


// CActionPage dialog

class CActionPage : public CPropertyPage
{
	DECLARE_DYNAMIC(CActionPage)

public:
	CActionPage();
	virtual ~CActionPage();

// Dialog Data
	enum { IDD = IDD_ACTION };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	virtual BOOL OnSetActive();
	virtual BOOL OnKillActive();
	virtual LRESULT OnWizardNext();
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnPaint();
};
