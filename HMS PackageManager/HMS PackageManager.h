// HMS PackageManager.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols

// CHMSPackageManagerApp:
// See HMS PackageManager.cpp for the implementation of this class
//

class CHMSPackageManagerApp : public CWinApp
{
public:
	CHMSPackageManagerApp();

	CString m_csPackage;
	CString m_csTempPath;

	CString m_csHMSPath;
	CString m_csDPPath;
	CString m_csMCPath;
	CString m_csDPath;

// Overrides
	public:
	virtual BOOL InitInstance();

// Implementation

	DECLARE_MESSAGE_MAP()
};

CString regGetStr(LPCSTR root,LPCSTR key, LPCSTR item, LPCSTR szDefault);
DWORD regGetInt(LPCSTR root,LPCSTR key, LPCSTR item,int def_value);
CString regGetStrLM(LPCSTR root,LPCSTR key, LPCSTR item, LPCSTR szDefault);

extern CHMSPackageManagerApp theApp;
