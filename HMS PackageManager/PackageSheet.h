#pragma once

#include "IntroPage.h"
#include "ActionPage.h"
#include "FinishPage.h"

// CPackageSheet

class CPackageSheet : public CPropertySheetEx
{
	DECLARE_DYNAMIC(CPackageSheet)

public:
	CPackageSheet(UINT nIDCaption, CWnd* pParentWnd = NULL, UINT iSelectPage = 0, HBITMAP hbmWatermark = NULL, HPALETTE hpalWatermark = NULL, HBITMAP hbmHeader = NULL);
	CPackageSheet(LPCTSTR pszCaption, CWnd* pParentWnd = NULL, UINT iSelectPage = 0, HBITMAP hbmWatermark = NULL, HPALETTE hpalWatermark = NULL, HBITMAP hbmHeader = NULL);
	virtual ~CPackageSheet();

protected:
	DECLARE_MESSAGE_MAP()

	void InitSheet(void);

	CIntroPage m_Intro;
	CActionPage m_Action;
	CFinishPage m_Finish;
};


