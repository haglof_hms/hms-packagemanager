#pragma once
#include "afxcmn.h"


// CMainDlg dialog

typedef struct _tagPROGRAM
{
	int nID;			// product-id
	int nProgtype;		// type of program
	CString csName;		// name of the program
	CString csLang;		// languages the program support

	int nKeys;			// num of keys
	int nLangs;			// num of langs
} PROGRAM;

typedef struct _tagLANGUAGE
{
	bool bLocal;		// does the file exist locally?
	CString csLang;		// name of the language
	int nVersion;		// version of the local file (if any)
	int nVersionWeb;	// version of the web file
	CString csPath;		// path to the program
	CString csPathWeb;	// webpath to the program
	CString csDesc;		// description about the program
	CString csDesc2;	// description about the new version
	CString csDate;
	CString csDateWeb;
	bool bVisible;		// visible or not
	
	CString csDoc;		// path to documentation
} LANGUAGE;

typedef struct _tagKEY
{
	int nKey;		// the key id
	int nSerial;	// serialnumber of hardware
	int nLevel;		// licenselevel
	int nCode;		// code
	CString csComment;	// comment
} KEY;


typedef struct _PACKAGEITEM
{
	CString csFilename;
	CString csType;
} PACKAGEITEM;


class CMainDlg : public CDialog
{
	DECLARE_DYNAMIC(CMainDlg)

public:
	CMainDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CMainDlg();

// Dialog Data
	enum { IDD = IDD_MAINDLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedInstall();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedOk();
	BOOL OnInitDialog();
	CProgressCtrl m_cProgress;

	// ---------------------------------------------------
	// data from package
	PROGRAM m_prgLocal;
	LANGUAGE m_lngLocal;

	CArray<PACKAGEITEM, PACKAGEITEM> m_caItems;
	// ---------------------------------------------------

	// data in XML
	CArray<PROGRAM, PROGRAM> m_caPrograms;
	CArray<LANGUAGE, LANGUAGE> m_caLanguages;
	CArray<KEY, KEY> m_caKeys;

	int m_nVersionXML;
	CString m_csDateXML;



	int IsHmsRunning();		// is HMS running?
	int ParsePackage();		// pre-parse the package.xml
	int InstallPackage();	// install all

	int LoadXML(CString);	// load a programs.xml
	int SaveXML(CString);	// save a programs.xml
};
