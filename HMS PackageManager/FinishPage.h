#pragma once

// CFinishPage : Property page dialog

class CFinishPage : public CPropertyPage
{
	DECLARE_DYNCREATE(CFinishPage)

// Constructors
public:
	CFinishPage();
	~CFinishPage();

// Dialog Data
	enum { IDD = IDD_FINISH };

// Implementation
protected:
	virtual void DoDataExchange(CDataExchange* pDX);        // DDX/DDV support

// Message maps
protected:
	DECLARE_MESSAGE_MAP()

	virtual BOOL OnSetActive();
	virtual BOOL OnKillActive();
	virtual LRESULT OnWizardNext();
	virtual BOOL OnInitDialog();
};
