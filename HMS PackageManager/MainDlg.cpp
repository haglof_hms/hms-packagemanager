// MainDlg.cpp : implementation file
//

#include "stdafx.h"
#include "HMS PackageManager.h"
#include "MainDlg.h"
#include "..\ZipArchive\ZipArchive.h"
#include "xmlParser.h"

// CMainDlg dialog

IMPLEMENT_DYNAMIC(CMainDlg, CDialog)

CMainDlg::CMainDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CMainDlg::IDD, pParent)
{
}

CMainDlg::~CMainDlg()
{
}

void CMainDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PROGRESS1, m_cProgress);
}


BEGIN_MESSAGE_MAP(CMainDlg, CDialog)
	ON_BN_CLICKED(ID_INSTALL, &CMainDlg::OnBnClickedInstall)
	ON_BN_CLICKED(IDCANCEL, &CMainDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDOK, &CMainDlg::OnBnClickedOk)
END_MESSAGE_MAP()


// CMainDlg message handlers
BOOL CMainDlg::OnInitDialog()
{
	SetDlgItemText(IDC_STATIC_INSTALL1, _T("Welcome to HMS PackageManager!"));

	if(theApp.m_csPackage == _T(""))
	{
		// pop a filedialog
		CString csFile;
		TCHAR tzFilename[512], tzDir[512];
		_stprintf(tzFilename, _T(""));
		_stprintf(tzDir, _T(""));

		// pop a filedialog
		CFileDialog dlgF(TRUE);
		dlgF.m_ofn.lpstrTitle = _T("Choose installationpackage");
		dlgF.m_ofn.lpstrFilter = (LPTSTR)_T("HMI files (*.hmi)\0*.hmi\0\0");
		dlgF.m_ofn.hwndOwner = AfxGetMainWnd()->GetSafeHwnd();
		dlgF.m_ofn.lpstrFile = tzFilename;
		dlgF.m_ofn.lpstrInitialDir = tzDir;

		int nRet = dlgF.DoModal();

		if(nRet == IDOK)
		{
			theApp.m_csPackage = dlgF.GetPathName();
		}
		else
		{
			SetDlgItemText(IDOK, _T("Aborted"));
			GetDlgItem(ID_INSTALL)->ShowWindow(FALSE);
			GetDlgItem(IDCANCEL)->ShowWindow(FALSE);
			GetDlgItem(IDOK)->ShowWindow(TRUE);

			return FALSE;
		}
	}


	CString csBuf;
	csBuf.Format(_T("Press Install to install %s, or Cancel to exit."), m_prgLocal.csName);
	SetDlgItemText(IDC_STATIC_INSTALL2, csBuf);

	CDialog::OnInitDialog();


	ParsePackage();

	csBuf.Format(_T("HMS PackageManager - %s v%d.%d"), m_prgLocal.csName, m_lngLocal.nVersion/10, m_lngLocal.nVersion%10);
	SetWindowText(csBuf);

	return TRUE;
}


void CMainDlg::OnBnClickedInstall()
{
	if(IsHmsRunning())
	{
		AfxMessageBox(_T("HMS is running, you need to quit that first!"));
		return;
	}

	if(InstallPackage())
	{
		SetDlgItemText(IDOK, _T("Done."));
	}
	else
	{
		SetDlgItemText(IDOK, _T("Error!"));
	}


	GetDlgItem(ID_INSTALL)->ShowWindow(FALSE);
	GetDlgItem(IDCANCEL)->ShowWindow(FALSE);
	GetDlgItem(IDOK)->ShowWindow(TRUE);
}


void CMainDlg::OnBnClickedCancel()
{
	CDialog::OnCancel();
}

void CMainDlg::OnBnClickedOk()
{
	CDialog::OnOK();
}

int CMainDlg::IsHmsRunning()
{
	if(FindWindow(_T("HMSShell"), NULL) == NULL)
		return FALSE;
	else
		return TRUE;
}

int CMainDlg::ParsePackage()
{
	CZipArchive zip;

	try
	{
		// open the package
		zip.Open(theApp.m_csPackage, CZipArchive::zipOpenReadOnly);
	
		// unpack the package.xml from the package
		int nIndex = zip.FindFile(_T("package.xml"), CZipArchive::ffNoCaseSens, true);
		if(nIndex != ZIP_FILE_INDEX_NOT_FOUND)
		{
			zip.ExtractFile(nIndex, theApp.m_csTempPath, false);

			// parse the package-xml
			XMLNode xNode = XMLNode::openFileHelper(theApp.m_csTempPath + _T("package.xml") ,"PMML");

			m_prgLocal.csLang = _T("");
			m_prgLocal.csName = _T("");
			m_prgLocal.nID = 0;
			m_prgLocal.nKeys = 0;
			m_prgLocal.nLangs = 0;
			m_prgLocal.nProgtype = 0;

			if(xNode.isEmpty() != 1)
			{
				XMLNode xChild;
				CString csBuf, csFile, csDest;
				int i, iterator=0;
				BOOL bDoc = FALSE;

				csBuf = xNode.getAttribute("id");
				if(csBuf != _T(""))
				{
					m_prgLocal.nID = atoi(csBuf);
				}

				csBuf = xNode.getAttribute("name");
				if(csBuf != _T(""))
				{
					m_prgLocal.csName = csBuf;
				}

				csBuf = xNode.getAttribute("lang");
				if(csBuf != _T(""))
				{
					m_lngLocal.csLang = csBuf;
				}

				csBuf = xNode.getAttribute("version");
				if(csBuf != _T(""))
				{
					m_lngLocal.nVersion = (int(atof(csBuf) * 10.0));
				}

				csBuf = xNode.getAttribute("description");
				if(csBuf != _T(""))
				{
					m_lngLocal.csDesc = csBuf;
				}

				csBuf = xNode.getAttribute("date");
				if(csBuf != _T(""))
				{
					m_lngLocal.csDate = csBuf;
				}

				csBuf = xNode.getAttribute("type");
				if(csBuf != _T(""))
				{
					if(csBuf == _T("DP"))
					{
						m_prgLocal.nProgtype = 0;
					}
					else if(csBuf == _T("MC"))
					{
						m_prgLocal.nProgtype = 1;
					}
					else if(csBuf == _T("D"))
					{
						m_prgLocal.nProgtype = 2;
					}
				}

				PACKAGEITEM item;
				m_caItems.RemoveAll();

				int n = xNode.nChildNode("file");
				for(i=0; i<n; i++)
				{
					xChild = xNode.getChildNode("file", &iterator);

					// extract the files found in the xml file.
					csBuf = xChild.getAttribute("type");
					item.csType = csBuf;

					csFile = xChild.getAttribute("name");
					item.csFilename = csFile;

					nIndex = zip.FindFile(csFile, CZipArchive::ffNoCaseSens, true);
					if(nIndex != ZIP_FILE_INDEX_NOT_FOUND)
					{
						// save to the file-array
						m_caItems.Add(item);
					}

				}
			}
			else
				throw(0);


			DeleteFile(theApp.m_csTempPath + _T("package.xml"));
		}

		zip.Close();
	}
	
	catch(...)
	{
		AfxMessageBox(_T("Exception in ParsePackage()!"));
		zip.Close(CZipArchive::afAfterException);
		return FALSE;
	}

	return TRUE;
}

int CMainDlg::InstallPackage()
{
	CZipArchive zip;
	PACKAGEITEM item;
	int nIndex;

	try
	{
		// open the package
		zip.Open(theApp.m_csPackage, CZipArchive::zipOpenReadOnly);

		// fix the buttons and stuff
		GetDlgItem(ID_INSTALL)->EnableWindow(FALSE);
		GetDlgItem(IDCANCEL)->EnableWindow(FALSE);
		GetDlgItem(IDC_PROGRESS1)->ShowWindow(TRUE);

		m_cProgress.SetRange32(0, 100);
		m_cProgress.SetPos(0);
		m_cProgress.SetStep((100/m_caItems.GetSize()));
		
		
		CString csPath;
		if(m_prgLocal.nProgtype == 0)	// DP
		{
			csPath.Format(_T("%s%d"), theApp.m_csDPPath, m_prgLocal.nID);
			CreateDirectory(csPath, NULL);

			LoadXML(theApp.m_csDPPath + _T("programs.xml"));
		}
		else if(m_prgLocal.nProgtype == 1)	// MC
		{
			csPath.Format(_T("%s%d"), theApp.m_csMCPath, m_prgLocal.nID);
			CreateDirectory(csPath, NULL);

			LoadXML(theApp.m_csMCPath + _T("programs.xml"));
		}
		else if(m_prgLocal.nProgtype == 2)	// D
		{
			csPath.Format(_T("%s%d"), theApp.m_csDPath, m_prgLocal.nID);
			CreateDirectory(csPath, NULL);
		}
		else if(m_prgLocal.nProgtype == 3)	// Report?
		{
		}
		else
			throw(0);

		for(int nLoop=0; nLoop<m_caItems.GetSize(); nLoop++)
		{
			// unpack/fix all the files
			item = m_caItems.GetAt(nLoop);

			nIndex = zip.FindFile(item.csFilename, CZipArchive::ffNoCaseSens, true);
			if(nIndex != ZIP_FILE_INDEX_NOT_FOUND)
			{
				// install/fix
				if(item.csType == _T("DP"))
				{
					m_lngLocal.csPath = item.csFilename;
				}
				else if(item.csType == _T("DPDOC"))
				{
					m_lngLocal.csDoc = item.csFilename;
				}
				else if(item.csType == _T("GENERIC"))
				{
				}

				zip.ExtractFile(nIndex, csPath, false);
//				DeleteFile(theApp.m_csTempPath + item.csFilename);
			}

			m_cProgress.StepIt();
		}

		zip.Close();

		m_lngLocal.bLocal = TRUE;

		if(m_prgLocal.nProgtype == 0)	// DP
		{
			// add program into PROGRAMS.XML
			SaveXML(theApp.m_csDPPath + _T("programs.xml"));
		}
		else if(m_prgLocal.nProgtype == 1)	// MC
		{
			// add program into PROGRAMS.XML
			SaveXML(theApp.m_csMCPath + _T("programs.xml"));
		}

		m_cProgress.SetPos(100);
	}

	catch(...)
	{
		AfxMessageBox(_T("Exception in InstallPackage()!"));
		zip.Close(CZipArchive::afAfterException);
		return FALSE;
	}



	return TRUE;
}

int CMainDlg::SaveXML(CString csFilename)
{
	CFile fh;
	char szBuf[8012];

	// make a backup-copy of the xml first, in case of failure
	CopyFile(csFilename, csFilename + _T(".bak"), FALSE);


	// search for the program and add the new info
	BOOL bFound = FALSE;
	int nLangIndex = 0;
	int nLoop=0, nLoop2=0;
	PROGRAM prg;
	LANGUAGE lng;

	for(nLoop=0; nLoop<m_caPrograms.GetSize(); nLoop++)
	{
		prg = m_caPrograms.GetAt(nLoop);

		if(prg.nID == m_prgLocal.nID)	// same program?
		{
			bFound = TRUE;
			break;
		}

		nLangIndex += prg.nLangs;
	}

	if(bFound == TRUE)	// we have found the program
	{
		bFound = FALSE;

		for(nLoop2=0; nLoop2<prg.nLangs; nLoop2++)
		{
			lng = m_caLanguages.GetAt(nLangIndex + nLoop2);

			if(lng.csLang == m_lngLocal.csLang)
			{
				bFound = TRUE;
				break;
			}
		}

		// have we found the language?
		if(bFound == TRUE)
		{
			m_caLanguages.SetAt(nLangIndex + nLoop2, m_lngLocal);
		}
		else
		{
			m_caLanguages.InsertAt(nLangIndex + nLoop2, m_lngLocal);
			prg.nLangs++;
		}

		// update the program
		m_caPrograms.SetAt(nLoop, prg);
	}
	else if(bFound == FALSE)	// program doesn't exist in local file.
	{
		m_caLanguages.InsertAt(nLangIndex + nLoop2, m_lngLocal);
		m_prgLocal.nLangs = 1;

		// add it
		m_caPrograms.Add(m_prgLocal);
	}


	if(fh.Open(csFilename, CFile::modeCreate|CFile::modeWrite) != 0)
	{
		PROGRAM prg;
		KEY key;
		LANGUAGE lng;
		int nKey = 0, nLoop2, nLoop3, nLang=0;

		sprintf(szBuf, "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\r\n<Programs>\r\n\t<Path>%s</Path>\r\n\t<Version>%d</Version>\r\n\t<Date>%s</Date>\r\n",
			csFilename, m_nVersionXML, m_csDateXML);
		fh.Write(szBuf, strlen(szBuf));

		for(int nLoop=0; nLoop<m_caPrograms.GetSize(); nLoop++)	// loop through all programs
		{
			prg = m_caPrograms.GetAt(nLoop);

			sprintf(szBuf, "\t<Product>\r\n\t\t<ID>%d</ID>\r\n\t\t<Name>%s</Name>\r\n\t\t<Progtype>%d</Progtype>\r\n",
				prg.nID,
				prg.csName,
				prg.nProgtype);
			fh.Write(szBuf, strlen(szBuf));

			if(prg.nLangs != 0)
			{
				for(nLoop3=nLang; nLoop3<(nLang+prg.nLangs); nLoop3++)
				{
					lng = m_caLanguages.GetAt(nLoop3);

					sprintf(szBuf, "\t\t<Lang>\r\n\t\t\t<ID>%s</ID>\r\n\t\t\t<Version>%d.%d</Version>\r\n\t\t\t<Local>%d</Local>\r\n\t\t\t<Filename>%s</Filename>\r\n\t\t\t<Doc>%s</Doc>\r\n\t\t\t<Description>%s</Description>\r\n\t\t\t<Date>%s</Date>\r\n\t\t</Lang>\r\n",
								   lng.csLang,
								   lng.nVersion/10, lng.nVersion%10,
								   lng.bLocal,
								   lng.csPath,
								   lng.csDoc,
								   lng.csDesc,
								   lng.csDate);
					fh.Write(szBuf, strlen(szBuf));
				}

				nLang += prg.nLangs;
			}

			if(prg.nKeys != 0)
			{
				for(nLoop2=nKey; nLoop2<(nKey+prg.nKeys); nLoop2++)
				{
					key = m_caKeys.GetAt(nLoop2);

					sprintf(szBuf, "\t\t<Key>\r\n\t\t\t<ID>%d</ID>\r\n\t\t\t<Level>%d</Level>\r\n\t\t\t<Serial>%04d</Serial>\r\n\t\t\t<Code>%04d</Code>\r\n\t\t\t<Comment>%s</Comment>\r\n\t\t</Key>\r\n", 
						key.nKey, key.nLevel, key.nSerial, key.nCode, key.csComment);
					fh.Write(szBuf, strlen(szBuf));
				}
				nKey += prg.nKeys;
			}

			sprintf(szBuf, "\t</Product>\r\n");
			fh.Write(szBuf, strlen(szBuf));
		}

		sprintf(szBuf, "</Programs>\r\n");
		fh.Write(szBuf, strlen(szBuf));

		fh.Close();
	}

	return 0;
}

int CMainDlg::LoadXML(CString csFilename)
{
	m_caPrograms.RemoveAll();
	m_caLanguages.RemoveAll();
	m_caKeys.RemoveAll();


	XMLNode xNode = XMLNode::openFileHelper(csFilename ,"PMML");

	if(xNode.isEmpty() != 1)
	{
		PROGRAM prg;
		KEY key;
		LANGUAGE lng;
		CString csID, csName, csVersion, csVersionWeb, csPath, csDesc, csBuf;
		XMLNode xChild, xKey, xLang;
		int i, iterator=0, j, jterator=0, nKeys=0, nLangs=0, nProgtype;

		// get the version and date of the XML
		csBuf = xNode.getChildNode("Version").getText();
		if(csBuf != _T("")) m_nVersionXML = atoi(csBuf);

		csBuf = xNode.getChildNode("Date").getText();
		if(csBuf != _T("")) m_csDateXML = csBuf;


		int n = xNode.nChildNode("Product");
		for(i=0; i<n; i++)
		{
			prg.nKeys = 0;
			prg.nLangs = 0;

			xChild = xNode.getChildNode("Product", &iterator);
			csID = xChild.getChildNode("ID").getText();
			csName = xChild.getChildNode("Name").getText();
			csBuf = xChild.getChildNode("Progtype").getText();
			if(csBuf != "") nProgtype = atoi(csBuf);

			nLangs = xChild.nChildNode("Lang");
			if(nLangs != 0)	// we have some langs
			{
				for(j=0; j<nLangs; j++)
				{
					xLang = xChild.getChildNode("Lang", &jterator);

					lng.csLang = xLang.getChildNode("ID").getText();
					csBuf = xLang.getChildNode("Version").getText();
					if(csBuf != "") lng.nVersion = (atof(csBuf) * 10.0);
					else lng.nVersion = 0;
					lng.nVersionWeb = 0;
					lng.csPath = xLang.getChildNode("Filename").getText();
					lng.csPathWeb = _T("");
					lng.csDesc = xLang.getChildNode("Description").getText();
					lng.csDate = xLang.getChildNode("Date").getText();
					csBuf = xLang.getChildNode("Local").getText();
					if(csBuf != "") lng.bLocal = atoi(csBuf);
					else lng.bLocal = FALSE;
					csBuf = xLang.getChildNode("Visible").getText();
					if(csBuf != "") lng.bVisible = atoi(csBuf);
					else lng.bVisible = TRUE;

					lng.csDoc = _T("");
					lng.csDoc = xLang.getChildNode("Doc").getText();

					if(lng.bLocal == FALSE)
					{
						lng.nVersionWeb = lng.nVersion;
						lng.nVersion = 0;
					}


					m_caLanguages.Add(lng);
					prg.nLangs++;
				}

				jterator = 0;
			}

			nKeys = xChild.nChildNode("Key");
			if(nKeys != 0)	// we have some keys
			{
				for(j=0; j<nKeys; j++)
				{
					xKey = xChild.getChildNode("Key", &jterator);
					key.nKey = 0;
					key.nLevel = 0;
					key.nSerial = 0;
					key.nCode = 0;
					key.csComment = _T("");

					csBuf = xKey.getChildNode("ID").getText();
					if(csBuf != "") key.nKey = atoi(csBuf);

					csBuf = xKey.getChildNode("Level").getText();
					if(csBuf != "") key.nLevel = atoi(csBuf);
					
					csBuf = xKey.getChildNode("Serial").getText();
					if(csBuf != "") key.nSerial = atoi(csBuf);

					csBuf = xKey.getChildNode("Code").getText();
					if(csBuf != "") key.nCode = atoi(csBuf);

					csBuf = xKey.getChildNode("Comment").getText();
					if(csBuf != "") key.csComment = csBuf;

					m_caKeys.Add(key);
					prg.nKeys++;
				}

				jterator = 0;
			}

			prg.csName = csName;
			prg.nID = atoi(csID);
			prg.nProgtype = nProgtype;
			m_caPrograms.Add(prg);
		}
	}
	else	// an error occured?
	{
	}

	return 0;
}
