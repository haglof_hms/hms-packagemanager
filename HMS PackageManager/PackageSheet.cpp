// PackageSheet.cpp : implementation file
//

#include "stdafx.h"
#include "HMS PackageManager.h"
#include "PackageSheet.h"


// CPackageSheet

IMPLEMENT_DYNAMIC(CPackageSheet, CPropertySheet)

CPackageSheet::CPackageSheet(UINT nIDCaption, CWnd* pParentWnd, UINT iSelectPage,
	HBITMAP hbmWatermark, HPALETTE hpalWatermark, HBITMAP hbmHeader)
	:CPropertySheet(nIDCaption, pParentWnd, iSelectPage,
				  hbmWatermark, hpalWatermark, hbmHeader)
{
	InitSheet();
}

CPackageSheet::CPackageSheet(LPCTSTR pszCaption, CWnd* pParentWnd, UINT iSelectPage,
	HBITMAP hbmWatermark, HPALETTE hpalWatermark, HBITMAP hbmHeader)
	:CPropertySheet(pszCaption, pParentWnd, iSelectPage,
					  hbmWatermark, hpalWatermark, hbmHeader)
{
	InitSheet();
}

CPackageSheet::~CPackageSheet()
{
}

void CPackageSheet::InitSheet(void)
{
	SetWizardMode();

	AddPage(&m_Intro);
	m_Intro.m_psp.dwFlags &= ~PSP_HASHELP;

	AddPage(&m_Action);
	m_Action.m_psp.dwFlags &= ~PSP_HASHELP;

	AddPage(&m_Finish);
	m_Finish.m_psp.dwFlags &= ~PSP_HASHELP;


	m_psh.dwFlags &= ~(PSH_HASHELP);
	m_psh.dwFlags |= PSH_WIZARD97|PSH_WATERMARK|PSH_HEADER;
//	m_psh.pszbmWatermark = MAKEINTRESOURCE(IDB_WATERMARK);
	m_psh.pszbmHeader = MAKEINTRESOURCE(IDB_BANNER);

	m_psh.hInstance = AfxGetInstanceHandle(); 
}


BEGIN_MESSAGE_MAP(CPackageSheet, CPropertySheet)
END_MESSAGE_MAP()


// CPackageSheet message handlers
