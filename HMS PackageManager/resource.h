//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by HMS PackageManager.rc
//
#define IDD_HMSPACKAGEMANAGER_DIALOG    102
#define IDD_MAINDLG                     102
#define IDD_INTRO                       129
#define IDD_ACTION                      130
#define IDD_FINISH                      131
#define IDB_BANNER                      132
#define IDB_WATERMARK                   135
#define IDS_INSTALL1                    136
#define IDS_INSTALL2                    137
#define IDR_MAINFRAME                   137
#define IDS_DONE                        138
#define IDS_INSTALL                     139
#define IDS_CANCEL                      140
#define IDS_ERROR                       141
#define IDC_PROGRESS1                   1000
#define ID_INSTALL                      1001
#define IDC_STATIC_INSTALL1             1003
#define IDC_STATIC_INSTALL2             1004

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        138
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1005
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
