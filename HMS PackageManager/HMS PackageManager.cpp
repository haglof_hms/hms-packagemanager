// HMS PackageManager.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "HMS PackageManager.h"
//#include "PackageSheet.h"
#include "MainDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CHMSPackageManagerApp

BEGIN_MESSAGE_MAP(CHMSPackageManagerApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// CHMSPackageManagerApp construction

CHMSPackageManagerApp::CHMSPackageManagerApp()
{
	m_csPackage = _T("");
	m_csTempPath = _T("");
}


// The one and only CHMSPackageManagerApp object
CHMSPackageManagerApp theApp;


// CHMSPackageManagerApp initialization

BOOL CHMSPackageManagerApp::InitInstance()
{
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinApp::InitInstance();

	SetRegistryKey(_T("Haglof"));
	CString csSection = _T("Settings");

	LPCTSTR m_pszOldProfileName = _tcsdup(m_pszProfileName);
	free((void*)m_pszProfileName);
	m_pszProfileName = _tcsdup(_T("HMS PackageManager"));

	TCHAR tzPath[MAX_PATH];
	GetTempPath(MAX_PATH, tzPath);
	m_csTempPath = tzPath;

	m_csPackage = __targv[1];

	// get HMS registry-keys
	m_csHMSPath = regGetStrLM(_T("Software\\HaglofManagmentSystem"), _T("Settings"), _T("HMSDir"), _T(""));
	m_csDPPath = regGetStr(_T("Software\\HaglofManagmentSystem"), _T("HMS_Communication\\DigitechPro"), _T("Filepath"), _T(""));
	m_csMCPath = regGetStr(_T("Software\\HaglofManagmentSystem"), _T("HMS_Communication\\Mantax computer"), _T("Filepath"), _T(""));
	m_csDPath = regGetStr(_T("Software\\HaglofManagmentSystem"), _T("HMS_Communication\\Digitech"), _T("Filepath"), _T(""));

	if(m_csDPPath.Right(1) != '\\') m_csDPPath.Append("\\");
	if(m_csMCPath.Right(1) != '\\') m_csMCPath.Append("\\");
	if(m_csDPath.Right(1) != '\\') m_csDPath.Append("\\");

	if(m_csHMSPath == _T("") || m_csDPPath == _T("") || m_csMCPath == _T("") || m_csDPath == _T(""))
	{
		AfxMessageBox(_T("Could not find registry settings, are HMS really installed?"));
		return FALSE;
	}

	// show main-window
	CMainDlg dlg;	
	dlg.DoModal();

	return FALSE;
}

CString regGetStr(LPCSTR root,LPCSTR key, LPCSTR item, LPCSTR szDefault)
{
	DWORD dwDisp;
	HKEY hk;
	TCHAR szRoot[512];
	TCHAR szValue[512];
	DWORD dwKeySize = 510;

	memset(szValue, 0, 511);

	sprintf(szRoot,"%s\\%s", root, key);
	if (RegCreateKeyEx(HKEY_CURRENT_USER,
		szRoot,
		0,
		NULL,
		REG_OPTION_NON_VOLATILE,
		KEY_READ,
		NULL,
		&hk,
		&dwDisp) == ERROR_SUCCESS)
	{
		if(RegQueryValueEx(hk,
			item,
			0,
			NULL,
			(PBYTE)&szValue,
			&dwKeySize) != ERROR_SUCCESS)
		{
			RegCloseKey(hk);
			return szDefault;
		}
	}

	RegCloseKey(hk);

	return szValue;
}

DWORD regGetInt(LPCSTR root,LPCSTR key, LPCSTR item,int def_value)
{
	DWORD dwDisp;
	HKEY hk;
	TCHAR szRoot[127];
	DWORD dwKeySize = 255;
	DWORD nValue = def_value;

	sprintf(szRoot,"%s\\%s",root,key);
	if (RegCreateKeyEx(HKEY_CURRENT_USER,
		szRoot,
		0,
		NULL,
		REG_OPTION_NON_VOLATILE,
		KEY_READ,
		NULL,
		&hk,
		&dwDisp) == ERROR_SUCCESS)
	{

		RegQueryValueEx(hk, item, 0,NULL, 
			(LPBYTE)&nValue, 
			&dwKeySize); 	
	}

	RegCloseKey(hk);

	return nValue;
}

CString regGetStrLM(LPCSTR root,LPCSTR key, LPCSTR item, LPCSTR szDefault)
{
	DWORD dwDisp;
	HKEY hk;
	TCHAR szRoot[512];
	TCHAR szValue[512];
	DWORD dwKeySize = 510;

	memset(szValue, 0, 511);

	sprintf(szRoot,"%s\\%s", root, key);
	if (RegCreateKeyEx(HKEY_LOCAL_MACHINE,
		szRoot,
		0,
		NULL,
		REG_OPTION_NON_VOLATILE,
		KEY_READ,
		NULL,
		&hk,
		&dwDisp) == ERROR_SUCCESS)
	{
		if(RegQueryValueEx(hk,
			item,
			0,
			NULL,
			(PBYTE)&szValue,
			&dwKeySize) != ERROR_SUCCESS)
		{
			RegCloseKey(hk);
			return szDefault;
		}
	}

	RegCloseKey(hk);

	return szValue;
}
