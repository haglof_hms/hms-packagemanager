// FinishPage.cpp : implementation file
//

#include "stdafx.h"
#include "HMS PackageManager.h"
#include "FinishPage.h"


// CFinishPage dialog

IMPLEMENT_DYNAMIC(CFinishPage, CPropertyPage)

CFinishPage::CFinishPage()
	: CPropertyPage(CFinishPage::IDD)
{

}

CFinishPage::~CFinishPage()
{
}

void CFinishPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CFinishPage, CPropertyPage)
END_MESSAGE_MAP()


// CFinishPage message handlers
BOOL CFinishPage::OnInitDialog()
{
	CPropertyPage::OnInitDialog();

	return TRUE;
}

BOOL CFinishPage::OnSetActive()
{
	CPropertySheetEx* pSheet = (CPropertySheetEx*)GetParent();
	pSheet->SetWizardButtons( PSWIZB_FINISH );
	return CPropertyPage::OnSetActive();
}

BOOL CFinishPage::OnKillActive()
{
	return CPropertyPage::OnKillActive();
}

LRESULT CFinishPage::OnWizardNext()
{
	return CPropertyPage::OnWizardNext();
}
