#pragma once


// CIntroPage dialog

class CIntroPage : public CPropertyPage
{
	DECLARE_DYNAMIC(CIntroPage)

public:
	CIntroPage();
	virtual ~CIntroPage();

// Dialog Data
	enum { IDD = IDD_INTRO };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	virtual BOOL OnSetActive();
	virtual BOOL OnKillActive();
	virtual LRESULT OnWizardNext();
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()
};
